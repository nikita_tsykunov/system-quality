import unittest
import json
import csv as _csv
import csvvalidator
from xml.etree import ElementTree

# to check console output
from contextlib import redirect_stdout
import io

from src.bitmex_connector import BitMEXWebsocket, BitMEXRESTConnector

# next test cases are needed to satisfy basic 3 requirements
# they check only success way. later will follow test cases
# to insure test coverage

class WebsocketTestCase(unittest.TestCase):
    def setUp(self):
        self.ws = BitMEXWebsocket(symbol='XBTUSD')
        self.ws.subscribe('orderBookL2')


class OrderbookTestCase(WebsocketTestCase):
    def runTest(self):
        order_book = self.ws.market_depth()

        self.assertIsInstance(order_book, list)
        self.assertIsInstance(order_book[0], dict)
        self.assertIn('symbol', order_book[0])
        self.assertIn('id', order_book[0])
        self.assertIn('side', order_book[0])
        self.assertIn('size', order_book[0])
        self.assertIn('price', order_book[0])


class RestTestCase(unittest.TestCase):
    def setUp(self):
        rest_client = BitMEXRESTConnector(symbol='XBTUSD')
        self.bt = rest_client.get_method()


class ExchangeStatisticTestCase(RestTestCase):
    def runTest(self):
        stats = self.bt.stats()

        self.assertIsInstance(stats, list)
        self.assertIsInstance(stats[0], dict)
        self.assertIn('rootSymbol', stats[0])
        self.assertIn('currency', stats[0])
        self.assertIn('volume24h', stats[0])
        self.assertIn('turnover24h', stats[0])
        self.assertIn('openInterest', stats[0])
        self.assertIn('openValue', stats[0])


class FormatValidationTestCase(RestTestCase):

    def runTest(self):
        self.test_csv_validation()
        self.test_json_validation()
        self.test_xml_validation()

    def test_json_validation(self):
        stats = self.bt.stats()
        json.dumps(stats)

        self.assert_

    def test_csv_validation(self):
        field_names = ('rootSymbol',
                       'currency',
                       'volume24h',
                       'turnover24h',
                       'openInterest',
                       'openValue')

        validator = csvvalidator.CSVValidator(field_names)

        validator.add_header_check()
        for value in field_names:
            if value == 'rootSymbol' or value == 'currency':
                validator.add_value_check(value, str)
            else:
                validator.add_value_check(value, int)

        stats = self.bt.stats(_format='csv')
        problems = validator.validate(_csv.reader(stats.splitlines()[:-2]))

        self.assertEqual(len(problems), 0)

        self.assert_

    def test_xml_validation(self):
        stats = self.bt.stats(_format='xml')

        ElementTree.fromstring(stats)

        self.assert_

# ---------------------------------------------
# next go test cases to improve test coverage
# ---------------------------------------------

#no connection for OrderBookL2
# the case when there is no connection is when internet connection absent
# doing so we won't be able to run other tests which require it
# so the solution is to explicitly close the socket


class WebsocketTestCaseNoConnection(unittest.TestCase):
    def setUp(self):
        self.ws = BitMEXWebsocket(symbol='XBTUSD')
        self.ws.exit()


class WebsocketTestCaseNoSubscription(unittest.TestCase):
    def setUp(self):
        self.ws = BitMEXWebsocket(symbol='XBTUSD')


class OrderbookTestCaseNoConnection(WebsocketTestCaseNoConnection):
    def runTest(self):
        f = io.StringIO()
        with redirect_stdout(f):
            order_book = self.ws.market_depth()
        self.assertEquals(f.getvalue(), 'Not connected to websocket\n')


class OrderbookTestCaseNoSubscription(WebsocketTestCaseNoSubscription):
    def runTest(self):
        f = io.StringIO()
        with redirect_stdout(f):
            order_book = self.ws.market_depth()
        self.assertEquals(f.getvalue(), 'You must subscribe to "orderBookL2" first\n')


class OrderbookTestCaseSubscriptionWithoutConnection(WebsocketTestCaseNoConnection):
    def runTest(self):
        f = io.StringIO()
        with redirect_stdout(f):
            self.ws.subscribe('orderBookL2')
        self.assertEquals(f.getvalue(), 'Websocket is not connected\n')


if __name__ == '__main__':
    unittest.main()
