from test import test
import unittest


def run():
    md = test.__dict__
    tests = [c() for c in md.values() if (isinstance(c, type) and
                                          c.__module__ == test.__name__ and
                                          'runTest' in dir(c))]

    test_suite = unittest.TestSuite()
    test_suite.addTests(tests)
    runner = unittest.runner.TextTestRunner()

    runner.run(test_suite)


if __name__ == '__main__':
    run()
