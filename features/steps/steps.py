from behave import *
import urllib3
import json
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

# -----------------------------------announcement--------


@when('Send announcement request')
def step_impl(context):
    context.http = urllib3.PoolManager()
    context.r = context.http.request('GET', 'https://testnet.bitmex.com/api/v1/announcement')


@then('Announcement correct form')
def step_impl(context):
    assert context.r.status is 200
    isinstance(context.r.data, list)
    isinstance(context.r.data[0], dict)

    data = json.loads(context.r.data.decode('utf-8'))
    assert 'id' in data[0]
    assert 'link' in data[0]
    assert 'title' in data[0]
    assert 'content' in data[0]
    assert 'date' in data[0]


# ------------------------------------funding---------------

@when('Send funding request')
def step_impl(context):
    context.http = urllib3.PoolManager()
    context.r = context.http.request('GET', 'https://testnet.bitmex.com/api/v1/funding?symbol=XBT&count=100&reverse=false')


@then('Funding correct form')
def step_impl(context):
    assert context.r.status is 200
    isinstance(context.r.data, list)
    isinstance(context.r.data[0], dict)
    data = json.loads(context.r.data.decode('utf-8'))
    assert 'timestamp' in data[0]
    assert 'symbol' in data[0]
    assert 'fundingInterval' in data[0]
    assert 'fundingRate' in data[0]
    assert 'fundingRateDaily' in data[0]

# ------------------------------------insurance---------------


@when('Send insurance request')
def step_impl(context):
    context.http = urllib3.PoolManager()
    context.r = context.http.request('GET', 'https://testnet.bitmex.com/api/v1/insurance?count=100&reverse=false')


@then('Insurance correct form')
def step_impl(context):
    assert context.r.status is 200
    isinstance(context.r.data, list)
    isinstance(context.r.data[0], dict)
    data = json.loads(context.r.data.decode('utf-8'))
    assert 'currency' in data[0]
    assert 'timestamp' in data[0]
    assert 'walletBalance' in data[0]

# ------------------------------------trade requests---------------


@when('Send trade request')
def step_impl(context):
    context.http = urllib3.PoolManager()
    context.r = context.http.request('GET', 'https://testnet.bitmex.com/api/v1/trade?count=100&reverse=false')


@then('Trade correct form')
def step_impl(context):
    assert context.r.status is 200
    isinstance(context.r.data, list)
    isinstance(context.r.data[0], dict)
    data = json.loads(context.r.data.decode('utf-8'))
    assert 'timestamp' in data[0]
    assert 'symbol' in data[0]
    assert 'side' in data[0]
    assert 'size' in data[0]
    assert 'price' in data[0]
    assert 'tickDirection' in data[0]
    assert 'trdMatchID' in data[0]
    assert 'grossValue' in data[0]
    assert 'homeNotional' in data[0]
    assert 'foreignNotional' in data[0]