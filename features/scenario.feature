Feature: Gets
    Scenario: Announcements on request
        When Send announcement request
        Then Announcement correct form

    Scenario: Funding on request
        When Send funding request
        Then Funding correct form

    Scenario: Insurance on request
        When Send insurance request
        Then Insurance correct form

    Scenario: Trade requests on request
        When Send trade request
        Then Trade correct form

