import websocket
import threading
import json
import requests

from time import sleep


class BitMEXWebsocket:

    MAX_TABLE_LEN = 200

    def __init__(self, symbol):
        self.data = dict()
        self.keys = dict()

        self.symbol = symbol
        self.url = 'wss://testnet.bitmex.com/realtime'

        self.subscribed_channels = set()

        self.is_connected = False

        self.__connect()

    def __del__(self):
        self.exit()

    def find_by_keys(self, table, match_data):
        for item in self.data[table]:
            if all(item[k] == match_data[k] for k in self.keys[table]):
                return item

    def subscribe(self, channel):
        self.__send_command(json.dumps({'op': 'subscribe', 'args': [f'{channel}:{self.symbol}']}))
        self.subscribed_channels.add(channel)

    def market_depth(self):
        if not self.is_connected:
            print('Not connected to websocket')
            return

        if 'orderBookL2' in self.subscribed_channels:
            if 'orderBookL2' not in self.data:
                self.__wait_for_data('orderBookL2')

            return self.data['orderBookL2']

        else:
            print('You must subscribe to "orderBookL2" first')

    def exit(self):
        if self.is_connected:
            self.is_connected = False
            self.ws.close()

    def __connect(self):
        self.ws = websocket.WebSocketApp(self.url, on_message=self.__on_message)

        self.wst = threading.Thread(target=lambda: self.ws.run_forever())
        self.wst.daemon = True
        self.wst.start()

        conn_timeout = 5
        while not self.ws.sock or not self.ws.sock.connected and conn_timeout:
            sleep(1)
            conn_timeout -= 1
        if not conn_timeout:
            self.ws.close()
            raise websocket.WebSocketTimeoutException('Couldn\'t connect to WS! Exiting.')

        self.is_connected = True

    def __on_message(self, message):
        message = json.loads(message)

        if 'subscribe' in message and message['success']:
            print(f'Successfully subscribed to {message["subscribe"]}')
            return

        if 'info' in message:
            return

        table = message.get('table')
        action = message.get('action')

        try:
            if table not in self.data:
                self.data[table] = []

            # There are four possible actions from the WS:
            # 'partial' - full table image
            # 'insert'  - new row
            # 'update'  - update row
            # 'delete'  - delete row
            if action == 'partial':
                self.data[table] += message['data']
                self.keys[table] = message['keys']

            elif action == 'insert':
                self.data[table] += message['data']

                if len(self.data[table]) > BitMEXWebsocket.MAX_TABLE_LEN:
                    self.data[table] = self.data[table][BitMEXWebsocket.MAX_TABLE_LEN // 2:]

            elif action == 'update':
                for updateData in message['data']:
                    item = self.find_by_keys(table, updateData)
                    if not item:
                        return
                    item.update(updateData)
                    if table == 'order' and item['leavesQty'] <= 0:
                        self.data[table].remove(item)

            elif action == 'delete':
                for deleteData in message['data']:
                    item = self.find_by_keys(table, deleteData)
                    self.data[table].remove(item)

            else:
                raise Exception("Unknown action: %s" % action)

        except Exception as e:
            print(e)

    def __send_command(self, data):
        if self.is_connected:
            self.ws.send(data)
        else:
            print('Websocket is not connected')

    def __wait_for_data(self, channel):
        while not {channel} <= set(self.data):
            sleep(0.1)


class BitMEXRESTConnector:
    def __init__(self, symbol):
        self.url = 'https://testnet.bitmex.com/api/v1/'
        self.symbol = symbol
        self.http = requests.session()

    def get_method(self):
        return BitmexMethod(self)

    def method(self, method, values=None):
        values = values.copy() if values else {}

        response = self.http.get(f'{self.url}{method}/?{"&".join(x[0] + "=" + x[1] for x in values.items())}')

        if response.ok:
            if '_format' not in values:
                response = response.json()
            elif values['_format'] == 'csv':
                response = response.content.decode('utf-8')
            elif values['_format'] == 'xml':
                response = response.content.decode('utf-8')
        else:
            response.raise_for_status()

        return response


class BitmexMethod(object):
    __slots__ = ('_bt', '_method')

    def __init__(self, bitmex, method=None):
        self._bt = bitmex
        self._method = method

    def __getattr__(self, method):
        if '_' in method:
            m = method.split('_')
            method = m[0] + '/'.join(i for i in m[1:])

        return BitmexMethod(
            self._bt,
            (self._method + '/' if self._method else '') + method
        )

    def __call__(self, **kwargs):
        return self._bt.method(self._method, kwargs)
